<?php namespace Keios\PGPaymill\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PGPaymill\Classes\PaymillChargeMaker;
use Keios\PaymentGateway\Core\Operator;
use Paymill\Request;
use Paymill\Models;
use Paymill\Services;

/**
 * Class Paymill
 *
 * @package Keios\PGPaymill
 */
class Paymill extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = true;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgpaymill::lang.operators.paymill';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgpaymill/assets/img/paymill/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $this->setApiKey();

        $chargeMaker = new PaymillChargeMaker($this->cart, $this->paymentDetails, $this->creditCard);

        $charge = $chargeMaker->make();

        try {
            $response = Charge::create($charge);

        } catch (\Paymill\Error\Card $ex) {

            $message = $this->makeCardErrorMessage($ex);

            return new PaymentResponse($this, null, [$message]);

        } catch (\Exception $e) {

            return new PaymentResponse($this, null, [$e->getMessage()]);
        }

        $this->isPaidInPaymill = $response['paid'];
        $this->chargeId = $response['id'];

        $internalRedirect = \URL::to(
            '_paymentgateway/' . OperatorUrlizer::urlize($this) . '?pgUuid=' . base64_encode($this->uuid)
        );

        return new PaymentResponse($this, $internalRedirect);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        if ($this->isPaidInPaymill && $this->can(Operator::TRANSITION_ACCEPT)) {
            try {
                $this->accept();
            } catch (\Exception $ex) {
                \Log::error($ex->getMessage());
            }

            return \Redirect::to($this->returnUrl);
        } else {
            return \Redirect::to($this->returnUrl);
        }
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        $this->setApiKey();

        try {

            $charge = Charge::retrieve($this->chargeId);
            $charge->refunds->create();

        } catch (\Exception $e) {

            return new PaymentResponse($this, null, [$e->getMessage()]);
        }

        // if no exception occured, we assume refund was created

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } else {
            throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
        }
    }

    protected function setApiKey()
    {
        $this->getSettings();

        $paymillApiKey = $this->getSettings()->get('paymill.apiKey');

        //PaymillApi::setApiKey($paymillApiKey);
        $request = new Paymill\Request($paymillApiKey);
    }

    protected function makeCardErrorMessage(\Paymill\Error\Card $exception)
    {
        $body = $exception->getJsonBody();
        $error = $body['error'];

        $errArr = [];

        $errArr['status'] = $e->getHttpStatus();
        array_key_exists('type', $error) ? $errArr['type'] = $error['type'] : false;
        array_key_exists('code', $error) ? $errArr['code'] = $error['code'] : false;
        array_key_exists('param', $error) ? $errArr['parameter'] = $error['param'] : false;
        array_key_exists('message', $error) ? $errArr['message'] = $error['message'] : false;

        $message = 'Error from Paymill: ';

        foreach ($errArr as $errKey => $errValue) {
            $message .= $errKey . ': ' . $errValue . ', ';
        }

        return $message;
    }
}
