<?php namespace Keios\PGPaymill;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PGPaymill Plugin Information File
 *
 * @package Keios\PGPaymill
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-Paymill',
            'description' => 'keios.pgpaymill::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-paymill'
        ];
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGPaymill\Operators\Paymill');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'paymill.general'  => [
                            'label' => 'keios.pgpaymill::lang.settings.general',
                            'tab'   => 'keios.pgpaymill::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'paymill.info'     => [
                            'type' => 'partial',
                            'path' => '@/plugins/keios/pgpaymill/partials/_paymill_info.htm',
                            'tab'  => 'keios.pgpaymill::lang.settings.tab',
                        ],
                        'paymill.apiKey'   => [
                            'label'   => 'keios.pgpaymill::lang.settings.apiKey',
                            'tab'     => 'keios.pgpaymill::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'paymill.testMode' => [
                            'label' => 'keios.pgpaymill::lang.settings.testMode',
                            'tab'   => 'keios.pgpaymill::lang.settings.tab',
                            'type'  => 'switch'
                        ],
                    ]
                );
            }
        );
    }
}
