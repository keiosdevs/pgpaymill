<?php namespace Keios\PGPaymill\Classes;

use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\ValueObjects\CreditCard;

class PaymillChargeMaker
{
    protected $cart;

    protected $details;

    protected $card;

    public function __construct(Orderable $cart, Details $details, CreditCard $card)
    {

        $this->cart = $cart;
        $this->details = $details;
        $this->card = $card;
    }

    public function make()
    {
        $cost = $this->cart->getTotalGrossCost(true);

        $parsedCard = $this->parseCard($this->card);

        $metaData = [
            'email' => $this->details->getEmail()
        ];

        $charge = [
            'amount'               => $cost->getAmount(),
            'currency'             => strtolower($cost->getCurrency()->getIsoCode()),
            'description'          => $this->details->getDescription(),
            'source'               => $parsedCard,
            'metadata'             => $metaData,
            'statement_descriptor' => $this->details->get('statement_descriptor', trim(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', \Request::getHost().' payment'))),
        ];

        if ($this->cart->hasShipping()) {
            $shipping = $this->cart->getShipping();
            $charge['shipping'] = [
                'name'     => $shipping->getName(),
                'cost'     => $shipping->getGrossCost()->getAmountBasic(),
                'currency' => $shipping->getGrossCost()->getCurrency()->getIsoCode(),
                'tax'      => $shipping->getTax().'%'
            ];
        }

        return $charge;

    }

    protected function parseCard(CreditCard $card)
    {
        return [
            'object'    => 'card',
            'number'    => $card->getNumber(),
            'exp_month' => (int)$card->getExpiryMonth(),
            'exp_year'  => $card->getExpiryYear(),
            'cvc'       => $card->getCvv(),
            'name'      => $card->getFirstName().' '.$card->getLastName()
        ];
    }
}