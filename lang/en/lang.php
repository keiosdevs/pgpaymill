<?php

return [
    'labels'    => [
        'pluginDesc' => 'Paymill Integration Extension for Keios PaymentGateway'
    ],
    'operators' => [
        'paymill' => 'Paymill'
    ],
    'settings'  => [
        'tab'      => 'Paymill',
        'general'  => 'General settings',
        'apiKey'   => 'You Paymill ApiKey',
        'testMode' => 'Use test mode'
    ],
    'info'      => [
        'header' => 'How to retrieve your Paymill api key'
    ]
];